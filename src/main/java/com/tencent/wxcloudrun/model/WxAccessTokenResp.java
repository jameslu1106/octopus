package com.tencent.wxcloudrun.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxAccessTokenResp implements Serializable {

    private static final long serialVersionUID = 202406012350002L;

    private String errcode;
    private String errmsg;
    private String access_token;
    private String expires_in;
}
