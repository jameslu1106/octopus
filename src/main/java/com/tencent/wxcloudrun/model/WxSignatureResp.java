package com.tencent.wxcloudrun.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxSignatureResp implements Serializable {

    private static final long serialVersionUID = 202406012355002L;

    private String appId;
    private Long timestamp;
    private String nonceStr;
    private String signature;
    private String url;

}
