package com.tencent.wxcloudrun.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxTicketResp implements Serializable {

    private static final long serialVersionUID = 202406012009001L;

    private String errcode;
    private String errmsg;
    private String ticket;
    private Long expires_in;
}
