package com.tencent.wxcloudrun.controller;

import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dao.WxTokenRecordMapper;
import com.tencent.wxcloudrun.model.WxSignatureResp;
import com.tencent.wxcloudrun.util.WeixinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * counter控制器
 */
@RestController
@CrossOrigin(origins = "https://wx.octopus360.cn")
public class WXController {

  private WxTokenRecordMapper wxTokenRecordDao;

  public WXController(@Autowired WxTokenRecordMapper wxTokenRecordMapper) {
    this.wxTokenRecordDao = wxTokenRecordMapper;
  }


  /**
   * 获取当前计数
   * @return API response json
   */
  @GetMapping(value = "/api/wx/token")
  public ApiResponse getWxToken(@RequestParam("url") String url) {

    WxSignatureResp wxSignatureResp = WeixinUtil.getSignature(url);
    return ApiResponse.ok(wxSignatureResp);
  }
  
}