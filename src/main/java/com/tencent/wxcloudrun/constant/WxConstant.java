package com.tencent.wxcloudrun.constant;

import org.apache.commons.lang3.tuple.Pair;


public class WxConstant {

    public static final String WX_APP_ID = "wxb760b68cad60f35c";

    public static final String WX_SECRET = "2eb73068c3a37a77a0e6404f89a059dd";

    public static Long wxTicketExpiresIn = 7200L;

    // left: wxticket; right: created time mills
    public static Pair<String, Long> wxTicketCache = Pair.of(null, 0L);
}
