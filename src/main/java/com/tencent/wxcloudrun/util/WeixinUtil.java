package com.tencent.wxcloudrun.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tencent.wxcloudrun.constant.WxConstant;
import com.tencent.wxcloudrun.model.WxAccessTokenResp;
import com.tencent.wxcloudrun.model.WxSignatureResp;
import com.tencent.wxcloudrun.model.WxTicketResp;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.util.Map;

public class WeixinUtil {

    public static ObjectMapper mapper = new ObjectMapper();
    private static String WX_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=%1$s&appid=%2$s&secret=%3$s";
    private static String WX_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=%1$s&access_token=%2$s";

    private static String NONCE_STR = "Wm3WZYTPz0wzccnW";

    private static String ENCRYPT_STR_PATTERN = "jsapi_ticket=%1$s&noncestr=%2$s&timestamp=%3$s&url=%4$s";

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static WxAccessTokenResp getAccessToken(String grantType, String appId, String secret) {

        String wxAccessTokenUrl = String.format(WX_ACCESS_TOKEN_URL, grantType, appId, secret);
        return excuteHttpGetRequest(wxAccessTokenUrl, null, WxAccessTokenResp.class);
    }

    public static WxTicketResp getTicket(String type, String accessToken) {

        String wxTicketUrl = String.format(WX_TICKET_URL, type, accessToken);
        return excuteHttpGetRequest(wxTicketUrl, null, WxTicketResp.class);
    }

    public static WxSignatureResp getSignature(String url) {
        Long timeStamp = System.currentTimeMillis()/ 1000;
        String str = String.format(ENCRYPT_STR_PATTERN,
                                   WxConstant.wxTicketCache.getLeft(),
                                   NONCE_STR,
                                   timeStamp,
                                   url);
        String signature = SHA1Helper.sha1(str);
        return new WxSignatureResp(WxConstant.WX_APP_ID, timeStamp, NONCE_STR, signature, url);
    }


    private static <T> T excuteHttpGetRequest(String url, Map<String, String> headers, Class<T> classes) {

        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpRequestBase httpRequest = new HttpGet(url);
            httpRequest.setConfig(RequestConfig.custom()
                    .setConnectionRequestTimeout(1000 * 30)
                    .setConnectTimeout(1000 * 30)
                    .setSocketTimeout(1000 * 30)
                    .build());
            if (null != headers && !headers.isEmpty()){
                for (String key : headers.keySet()){
                    httpRequest.addHeader(key, headers.get(key));
                }
            }
            try (CloseableHttpResponse response = client.execute(httpRequest)) {
                if (response.getEntity() == null) return null;
                String responseStr = EntityUtils.toString(response.getEntity());
                if (StringUtils.isBlank(responseStr)){
                    return null;
                }
                return mapper.readValue(responseStr, classes);
            }
        } catch (Exception ex){
            return null;
        }
    }
}
