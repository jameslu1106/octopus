package com.tencent.wxcloudrun.schedule;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tencent.wxcloudrun.constant.WxConstant;
import com.tencent.wxcloudrun.dao.WxTokenRecordMapper;
import com.tencent.wxcloudrun.model.WxAccessTokenResp;
import com.tencent.wxcloudrun.model.WxTicketResp;
import com.tencent.wxcloudrun.util.WeixinUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class WxScheduler {

    private WxTokenRecordMapper wxTokenRecordDao;

    public WxScheduler(@Autowired WxTokenRecordMapper wxTokenRecordMapper) {
        this.wxTokenRecordDao = wxTokenRecordMapper;
    }

    @Scheduled(cron = "0 */1 * * * ?")
    public void refreshWxTicket() {

        String ticket = WxConstant.wxTicketCache.getLeft();
        Long createdTimeMills = WxConstant.wxTicketCache.getRight();
        Long currentTimeMills = System.currentTimeMillis();
        // 提前5分钟重新生成ticket
        Long reCreateTicketInterval = (WxConstant.wxTicketExpiresIn - 5 * 60) * 1000;

        // not expired
        if (StringUtils.isNotBlank(ticket) && ((currentTimeMills - createdTimeMills) < reCreateTicketInterval)) {
            return;
        }

        // recreate access token
        WxAccessTokenResp wxAccessTokenResp = WeixinUtil.getAccessToken("client_credential", WxConstant.WX_APP_ID, WxConstant.WX_SECRET);
        String wxAccessTokenRespStr = null;
        if (null != wxAccessTokenResp) {
            try {
                wxAccessTokenRespStr = WeixinUtil.mapper.writeValueAsString(wxAccessTokenResp);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        this.wxTokenRecordDao.save("ACCESS_TOKEN", wxAccessTokenRespStr);
        if (null == wxAccessTokenResp && StringUtils.isBlank(wxAccessTokenResp.getAccess_token())) {
            return;
        }

        // recreate ticket
        WxTicketResp wxTicketResp = WeixinUtil.getTicket("jsapi", wxAccessTokenResp.getAccess_token());
        String wxTicketRespStr = null;
        if (null != wxTicketResp) {
            try {
                wxTicketRespStr = WeixinUtil.mapper.writeValueAsString(wxTicketResp);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        this.wxTokenRecordDao.save("TICKET", wxTicketRespStr);
        if (null == wxTicketResp && StringUtils.isBlank(wxTicketResp.getTicket())) {
            return;
        }
        // save ticket
        WxConstant.wxTicketCache = Pair.of(wxTicketResp.getTicket(), currentTimeMills);
        if (null != wxTicketResp.getExpires_in()) {
            WxConstant.wxTicketExpiresIn = wxTicketResp.getExpires_in();
        }
    }
}
